/*
 * My project.
 *
 * Copyright (c) 2017.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package self.edu.qna.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import self.edu.qna.model.Question;
import self.edu.qna.repository.QuestionJpaRepository;
import self.edu.qna.service.QuestionService;

/**
 * Service implementation for working with questions.
 */
@Service
public class QuestionServiceImpl implements QuestionService {

  @Autowired
  private QuestionJpaRepository questionRepository;

  /**
   * Searches question by id.
   *
   * @param id the question id.
   * @return the found question or null if no question with such id.
   */
  @Override
  public Question findQuestionById(Long id) {
    return questionRepository.findOne(id);
  }

  /**
   * SAves the question.
   *
   * @param question the question to save.
   * @return the same object with assigned id.
   */
  @Override
  public Question saveQuestion(Question question) {
    return questionRepository.save(question);
  }
}
