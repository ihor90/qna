/*
 * My project.
 *
 * Copyright (c) 2017.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package self.edu.qna.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import self.edu.qna.model.Question;
import self.edu.qna.service.QuestionService;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Rest API for questions.
 *
 * Handles interaction with user.
 * A user can save a question and then get it.
 * The link to saved question provides in the responce using HATEOAS.
 */
@RestController
@RequestMapping("/question")
public class QuestionController {

  @Autowired
  private QuestionService questionService;

  /**
   * Searches the question by id.
   *
   * Returns the question with the same link.
   *
   * @param id - the question id
   * @return the question containing self-link.
   */
  @RequestMapping("/{id}")
  public Question getQuestionById(@PathVariable Long id) {
    Question foundQuestion = questionService.findQuestionById(id);
    foundQuestion.add(linkTo(methodOn(QuestionController.class)
        .getQuestionById(id)).withSelfRel());
    return foundQuestion;
  }

  /**
   * API for saving a question.
   *
   * Saves the entered info by user. Returns
   * the saved question with the link to get it.
   *
   * @param question the question info
   * @return the question with self-link
   */
  @RequestMapping(value = "/create", method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.CREATED)
  public Question createQuestion(@RequestBody Question question) {
    Question savedQuestion = questionService.saveQuestion(question);
    savedQuestion.add(linkTo(methodOn(QuestionController.class)
        .getQuestionById(savedQuestion.getQuestionId()))
        .withSelfRel());
    return savedQuestion;
  }

}
