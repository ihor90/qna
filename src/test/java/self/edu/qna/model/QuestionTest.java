package self.edu.qna.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

@RunWith(JUnit4.class)
public class QuestionTest {

  @Test
  public void testPojo() {
    Class<?> questionClass = Question.class;
    assertPojoMethodsFor(questionClass).areWellImplemented();
  }

}