package self.edu.qna.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalTime;

import self.edu.qna.model.Question;
import self.edu.qna.repository.QuestionJpaRepository;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class QuestionControllerTest {

  private final ObjectMapper objectMapper = new ObjectMapper();
  @Autowired
  private MockMvc mvc;
  @Autowired
  private QuestionJpaRepository questionRepository;

  @Test
  public void testCreateQuestion() throws Exception {
    //when
    String title = "MyQuestion";
    String description = "My description" + LocalTime.now();
    Question question = new Question(title, description);
    mvc.perform(post("/question/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(question)))
        .andExpect(status().isCreated());

    //then
    Question foundQuestion = questionRepository.findByTitle(title);
    assertEquals("saved question should be the same",
        description, foundQuestion.getDescription());

    //when
    mvc.perform(get("/question/" + foundQuestion.getQuestionId()))
        //then
        .andExpect(status().isOk());
  }
}